import '../styles/index.scss';
import * as moment from 'moment';

var update = function() {
    document.getElementById("datetime").innerHTML = moment().format('H:mm:ss');
};
setInterval(update, 1000);


const calculator = {
    displayValue: '0',
    firstOperand: null,
    waitingForSecondOperand: false,
    operator: null,
};
  
function inputDigit(digit) {
    const { displayValue, waitingForSecondOperand } = calculator;
  
    if (waitingForSecondOperand === true) {
      calculator.displayValue = digit;
      calculator.waitingForSecondOperand = false;
    } else {
      calculator.displayValue = displayValue === '0' ? digit : displayValue + digit;
    }
}
  
function inputDecimal(dot) {
    // If the `displayValue` does not contain a decimal point
    if (!calculator.displayValue.includes(dot)) {
      // Append the decimal point
      calculator.displayValue += dot;
    }
}
  
  
function handleOperator(nextOperator) {
    const { firstOperand, displayValue, operator } = calculator;
    const inputValue = parseFloat(displayValue);
  
    if (operator && calculator.waitingForSecondOperand)  {
      calculator.operator = nextOperator;
      return;
    }
  
    if (firstOperand == null) {
      calculator.firstOperand = inputValue;
    } else if (operator) {
      const currentValue = firstOperand || 0;
      const result = performCalculation[operator](currentValue, inputValue);
  
      calculator.displayValue = String(result);
      calculator.firstOperand = result;
    }
  
    calculator.waitingForSecondOperand = true;
    calculator.operator = nextOperator;
}
  
const performCalculation = {
    '/': (firstOperand, secondOperand) => (firstOperand / secondOperand).toFixed(4),
  
    '*': (firstOperand, secondOperand) => firstOperand * secondOperand,
  
    '+': (firstOperand, secondOperand) => firstOperand + secondOperand,
  
    '-': (firstOperand, secondOperand) => firstOperand - secondOperand,
  
    '=': (firstOperand, secondOperand) => secondOperand
};
  
function resetCalculator() {
    calculator.displayValue = '0';
    calculator.firstOperand = null;
    calculator.waitingForSecondOperand = false;
    calculator.operator = null;
}
  
function updateDisplay() {
    const display = document.querySelector('.calculator-screen');
    display.value = calculator.displayValue;
}
  
updateDisplay();
  
const keys = document.querySelector('.calculator-keys');
keys.addEventListener('click', (event) => {
    const { target } = event;
    if (!target.matches('button')) {
      return;
    }
  
    if (target.classList.contains('operator')) {
      handleOperator(target.value);
          updateDisplay();
      return;
    }
  
    if (target.classList.contains('decimal')) {
      inputDecimal(target.value);
          updateDisplay();
      return;
    }
  
    if (target.classList.contains('all-clear')) {
      resetCalculator();
          updateDisplay();
      return;
    }
  
    inputDigit(target.value);
    updateDisplay();
});

document.getElementById("rock").addEventListener("click", rock);
document.getElementById("scissor").addEventListener("click", scissor);
document.getElementById("paper").addEventListener("click", paper);


function rock() {
    let rockVal = document.getElementById("rock").value;
    displayResult(rockVal);
};
function paper() {
    var paperVal = document.getElementById("paper").value;
    displayResult(paperVal);
};
function scissor() {
    let scissorVal = document.getElementById("scissor").value;
    displayResult(scissorVal);
};

//rock = 1, paper = 2, scissor = 3
var cpuScore = 0;
var playerScore = 0;
var displayResult = function(val) {
    let cpuVal = Math.floor((Math.random() * 3) + 1);
    if(val === "1" && cpuVal === 3 || val === "2" && cpuVal === 1 || val === "3" && cpuVal === 2) {
        playerScore++;
        document.getElementById("game-result").innerHTML = "<h2>You Won!!</h2><p> Player: " + playerScore + " - " + cpuScore + " :CPU";
    } else if (val === "1" && cpuVal === 2 || val === "2" && cpuVal === 3 || val === "3" && cpuVal === 1) {
        cpuScore++;
        document.getElementById("game-result").innerHTML = "<h2>You loose....</h2><p> Player: " + playerScore + " - " + cpuScore + " :CPU";
    } else {
        document.getElementById("game-result").innerHTML = "<h2>It's a draw...</h2><p> Player: " + playerScore + " - " + cpuScore + " :CPU";
    }

    if(val === "1") {
        document.getElementById("player").innerHTML = "<p>Player choice:</p><img src='src/img/Rock.PNG'>";
    } else if(val === "2") {
        document.getElementById("player").innerHTML = "<p>Player choice:</p><img src='src/img/Paper.PNG'>";
    } else if(val === "3") {
        document.getElementById("player").innerHTML = "<p>Player choice:</p><img src='src/img/Scissor.PNG'>";
    }

    if(cpuVal === 1) {
        document.getElementById("cpu").innerHTML = "<p>Cpu choice:</p><img src='src/img/Rock.PNG'>";
    } else if(cpuVal === 2) {
        document.getElementById("cpu").innerHTML = "<p>Cpu choice:</p><img src='src/img/Paper.PNG'>";
    } else if(cpuVal === 3) {
        document.getElementById("cpu").innerHTML = "<p>Cpu choice:</p><img src='src/img/Scissor.PNG'>";
    }
};
/**
 * For more information on using Moment head to https://momentjs.com/
 */