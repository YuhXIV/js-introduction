function fizzBuzz(fizz, buzz, fizzbuzz, len) {
    for (i = 1; i < len+1; i++) {
        if(i % fizz === 0) {
            console.log("number " + i + ": Fizz");
        }
        if(i % buzz === 0) {
            console.log("number " + i + ": Buzz");
        }
        if(i % fizzbuzz === 0) {
            console.log("number " + i + ": FizzBuzz");
        } 
    }
}

//fizzBuzz(3, 5, 15, 50);

function trueFizzBuzz(fizz, buzz, len) {
    for (i = 1; i < len+1; i++) {
        if(i % fizz === 0 && i % buzz === 0) {
            console.log("number " + i + ": FizzBuzz");
        } else if(i % fizz === 0) {
            console.log("number " + i + ": Fizz");
        } else if(i % buzz === 0) {
            console.log("number " + i + ": Buzz");
        } 
    }
}
trueFizzBuzz(3,5,50);