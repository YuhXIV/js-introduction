# JS introduction

## Part 1

Part 1 is in a folder called Part_1 in the location src/script.
Part 1 consist of 2 task, which is create fibonacci sequence with JavaScript and a FizzBuzz concept

### Task 1 - Math stuff
- [x] Write a small program to display the first 50 numbers in
the fibonacci sequence

### Task 2 - FizzBuzz

- [x] Write a small program to display the words Fizz and Buzz
- [x] Write a loop up to 50
- [x] If the current loop index is divisible by 3 - Display Fizz
- [x] If the current loop index is divisible by 5 - Display Buzz
- [x] If the current loop index is divisible by 15 - Display FizzBuzz

## Part 2

Part 2 consist of 5 task, but only 3 of the task is being handed in (in consideration of instruction in noroff webpage). All three task is smashed together in 1 html file with 1 css and js file. They are all called index.html/js/scss.

### Task 1 - JavaScript Clock

- [x] Build a JavaScript Clock
    - [x] Display the current time and date on a webpage
- [x] Use moment js
    - [x] Install it with npm
- [x] Use Webpack starter (Includes Moment)
 
### Task 2 - Calculator

- [x] Build a calculator using JavaScript and HTML
- [x] Use buttons for numbers and display the results on a web page
- [x] The user should be able to add, subtract, divide and multiply.
- [x] Check for rounding
- [x] Make it pretty-ish

### Task 3 - Rock/paper/scissor game

- [x] Build a rock papers scissors game
- [x] The user should choose their hand
- [x] The CPU “Player” should the respond with their hand
- [x] Rock Beats Scissor
- [x] Scissor Beats Paper
- [x] Paper Beats Rock